package backend


import grails.gorm.transactions.Transactional
import groovy.time.TimeCategory
import groovy.time.TimeDuration

import java.time.LocalDateTime

@Transactional
class StockService {

    def getStocks(Company company, int numbersOfHoursUntilNow) {
        LocalDateTime now = LocalDateTime.now().withMinute(0).withNano(0)
        LocalDateTime fromDate = now.minusHours(numbersOfHoursUntilNow)

        def timeStart = new Date()
        def list = getStocksFrom(company, fromDate)

        def stockPrice = []
        list.each { stock ->
            stockPrice << stock.price
        }
        def timeStop = new Date()
        TimeDuration duration = TimeCategory.minus(timeStop, timeStart)

        println 'stocks from ' + numbersOfHoursUntilNow + ' hours before'
        println 'stock from ' + fromDate.toString()
        println stockPrice.join('; ')
        println 'total time: ' + duration
        println 'total quotes: ' + list.size()
    }

    def getStocksFrom(Company company, LocalDateTime numbersOfHoursUntilNow) {
        Stock.where {
            priceDate >= numbersOfHoursUntilNow
            company == company
        }.list()
    }

    static standardDeviation(Company company) {
        if (!company) {
            throw new Exception("Company is required")
        }
        if (!Stock.findAllByCompany(company)) {
            throw new Exception("The Company has no stock history")
        }

        def totalAndAverage = getTotalAndAverageFromCompany(company)
        def total = totalAndAverage['total']
        def average = totalAndAverage['average']?.round(2) ?: new BigDecimal(0)
        def variance = getVariance(company, average, total)
        BigDecimal standardDeviation = Math.sqrt(variance)
        standardDeviation.round(5)
    }

    static getTotalAndAverageFromCompany(Company company) {
        Stock.getTotalAndAverageFromCompany(company)
    }

    static getVariance(Company company, def average, def total) {
        def result = Stock.subtractAverage(company, average)
        def variance = (result['sum'] / (total - 1))
        variance
    }
}
