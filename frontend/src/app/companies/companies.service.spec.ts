import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CompaniesService } from './companies.service';
import { HttpClient } from 'selenium-webdriver/http';

describe('should be create', () => {
  let injector: TestBed;
  let service: CompaniesService;
  let httpMock: HttpTestingController;

  beforeEach(async () => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [CompaniesService]
  }));

  beforeEach(() => {
    injector = getTestBed();
    service = injector.get(CompaniesService)
    httpMock = injector.get(HttpTestingController);
  })

  afterEach(() => {
    httpMock.verify();
  });  

  const companies = [
    { name: 'Tesla', segment: 'Automotive', 'standardDeviation': 5.00 }
  ]

  it('to able to retrieve comapanies from api', () => {
    service.getCompanies().subscribe((res) => {
      expect(res).toEqual(companies);
    });

    const req = httpMock.expectOne('http://localhost:8080/stock/companies');
    expect(req.request.method).toBe('GET');
    req.flush(companies);
  });
});
