package backend

import grails.gorm.transactions.Transactional

@Transactional
class CompanyService {

    def save(name, segment) {
        Company newCompany = new Company(name: name, segment: segment)
        newCompany.save()
        newCompany
    }
}
