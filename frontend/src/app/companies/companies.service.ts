import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  private API_URL = "http://localhost:8080"
  
  // companies: {}

  constructor(private http: HttpClient) {
    // this.companies = [
    //   { name: 'Tesla', segment: 'Automotive', standardDeviation: '2.30'},
    //   { name: 'Ford', segment: 'Automotive', standardDeviation: '22.30'},
    //   { name: 'Toyota', segment: 'Automotive', standardDeviation: '23.30'},
    // ]
   }

  getCompanies() {
    return this.http.get(this.API_URL + "/stock/companies");
  }
}
