package backend

import java.time.LocalDateTime

class Stock {

    Company company
    BigDecimal price
    LocalDateTime priceDate

    static constraints = {
        company blank: false, nullable: false
        price blank: false, nullable: false
        priceDate blank: false, nullable: false
    }

    static getTotalAndAverageFromCompany(Company company) {
        def result = Stock.executeQuery("SELECT count(*), (sum(price) / count(*)) FROM Stock where company_id = :companyId", [companyId: company.id])
        if (!result.get(0))
            throw new Exception("Company or stock not exists")

        [total: result.get(0)[0], average: result.get(0)[1]]
    }

    static subtractAverage(Company company, def average) {
        def result = Stock.executeQuery("select sum(((price - ${average}) * (price - ${average}))) " +
                "from Stock where company_id = :companyId", [companyId: company.id])

        if (result.size() != 1)
            throw new Exception("Company or stock not exists")

        [sum: result[0]]

    }

}
