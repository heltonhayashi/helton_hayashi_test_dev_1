package DTO

import backend.Company

class CompanyDto {

    String name
    String segment
    BigDecimal standardDeviation

    static mapWith = 'none'

    static of (Company company, BigDecimal standardVariation) {
        CompanyDto dto = new CompanyDto()
        dto.name = company.name
        dto.segment = company.segment
        dto.standardDeviation = standardVariation
        dto
    }

}
