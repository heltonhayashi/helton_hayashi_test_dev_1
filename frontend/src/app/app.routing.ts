import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CompaniesComponent } from './companies/companies.component';

const APP_ROUTES: Routes = [
    { path: '', component: AppComponent },
    { path: 'companies', component: CompaniesComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);