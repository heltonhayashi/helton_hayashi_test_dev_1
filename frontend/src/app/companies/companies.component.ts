import { Component, OnInit } from '@angular/core';
import { CompaniesService } from './companies.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent {

  companies: {};

  constructor(private companiesService: CompaniesService) {
  }

  ngOnInit() {
    this.companiesService.getCompanies().subscribe((data: any[])=>{
      this.companies = data;
    })
  }
}
