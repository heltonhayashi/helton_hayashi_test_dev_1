package backend

import DTO.CompanyDto
import grails.converters.JSON

class StockController {

    def stockService

    def index() {
        def data = stockService.getStocks(Company.findByName('Tesla'), 30)
        redirect(uri: '/')
    }

    def companies() {
        def allCompanies = Company.findAll()
        def list = []
        allCompanies.forEach { Company company ->
            try {
                BigDecimal standardDeviation = stockService.standardDeviation(company)
                list << CompanyDto.of(company, standardDeviation)
            } catch (Exception e) {
                list << CompanyDto.of(company, new BigDecimal(0))
            }
        }
        render list as JSON
    }
}
