package backend

import org.hibernate.Session
import org.hibernate.Transaction

import java.time.LocalDateTime

class BootStrap {

    CompanyService companyService
    def sessionFactory

    def init = { servletContext ->
        def tesla = companyService.save("Tesla", "Automotive")
        def google = companyService.save("Google", "Technology")
        def apple = companyService.save("Apple", "Technology")

        generateStockFrom(tesla, 100)
        generateStockFrom(apple, 300)
        generateStockFrom(google, 1300)
    }

    def generateStockFrom(Company company, def startPrice) {
        // a cada hora existem 60 registros 10:00 a 10:59
        // das 10 as 18h são 8 horas
        // 8 * 60 = 480 registros por dia
        def startHour = 10
        def daysCount = 1
        def price = startPrice

        LocalDateTime today = LocalDateTime.now().withHour(startHour).withMinute(0).withSecond(0).withNano(0)
        LocalDateTime date = today.minusDays(30)

        Session session = sessionFactory.openSession()
        Transaction tx = session.beginTransaction()

        while (daysCount <= 30) {
            if (!(date.getDayOfWeek().toString() in ['SATURDAY', 'SUNDAY'])) {
                (1..8).each { counterHour ->
                    (1..60).each { counterMinute ->
                        price = variation(price)
                        def stock = new Stock()
                        stock.company = company
                        stock.price = price
                        stock.priceDate = date
                        session.save(stock)
                        date = date.plusMinutes(1)
                    }
                    session.flush()
                    session.clear()
                }
                daysCount++
            }
            date = date.plusDays(1).withHour(startHour).withMinute(0)
        }
        tx.commit()
        session.close()
    }

    BigDecimal variation(def old_price) {
        def volatility = 0.02
        def random = Math.random()
        def change_percent = 2 * volatility * random
        if (change_percent > volatility)
            change_percent -= (2 * volatility)
        def change_amount = old_price * change_percent
        def new_price = old_price + change_amount
        if (new_price > 0) {
            return new_price
        } else {
            return variation(old_price)
        }
    }
}
