package backend

import grails.testing.gorm.DataTest
import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

import java.time.LocalDateTime

class StockServiceSpec extends Specification implements ServiceUnitTest<StockService>, DataTest {

    def setupSpec() {
        mockDomain Company
        mockDomain Stock
    }

    def setup() {
        def company = new Company(name: "Tesla", segment: "Auto").save()
        def company2 = new Company(name: "Ford", segment: "Auto").save()
        BigDecimal price = new BigDecimal(11)
        new Stock(company: company, price: price, priceDate: LocalDateTime.of(2020, 05, 01, 10, 00)).save()
        new Stock(company: company, price: price, priceDate: LocalDateTime.of(2020, 05, 01, 10, 00).minusHours(50)).save()

        new Stock(company: company2, price: price, priceDate: LocalDateTime.of(2020, 05, 01, 10, 00)).save()
        new Stock(company: company2, price: price, priceDate: LocalDateTime.of(2020, 05, 01, 17, 20)).save()
    }

    void "test getStocksFrom Tesla company, 30 hours expect one"() {
        given:
        def company = Company.findByName('Tesla')
        def list = service.getStocksFrom(company, LocalDateTime.of(2020, 05, 01, 10, 00).minusHours(30))

        expect: "list of one result"
        list.size() == 1
    }

    void "test getStocksFrom Tesla company, 60 hours expect two"() {
        given:
        def company = Company.findByName('Tesla')
        def list = service.getStocksFrom(company, LocalDateTime.of(2020, 05, 01, 10, 00).minusHours(60))

        expect: "list of two results"
        list.size() == 2
    }

    void "test getStocksFrom Ford company, 51 hours expect two"() {
        given:
        def company = Company.findByName('Ford')
        def list = service.getStocksFrom(company, LocalDateTime.of(2020, 05, 01, 15, 00))

        expect: "list of two results"
        list.size() == 1
    }

}
