package backend

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class CompanySpec extends Specification implements DomainUnitTest<Company> {

    def setup() {
        new Company(segment: "Auto").save(flush: true)
        new Company(name: "Tesla", segment: "Auto").save(flush: true)
    }

    void "test required fields"() {
        given:
        def company = Company.findByName('Tesla')

        expect:
        company.name == 'Tesla'
    }
}
