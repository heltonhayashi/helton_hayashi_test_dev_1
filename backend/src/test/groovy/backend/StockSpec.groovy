package backend

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

import java.time.LocalDateTime

class StockSpec extends Specification implements DomainUnitTest<Stock> {

    def setup() {
        def company = new Company(name: "Tesla", segment: "Auto").save()
        BigDecimal price = new BigDecimal(11)
        new Stock(company: company, price: price, priceDate: LocalDateTime.now()).save()
        new Stock(company: company, price: price, priceDate: LocalDateTime.now()).save()
    }

    void "test something"() {
        expect: "fix me"
        Stock.count() == 2
    }
}
